# Contributing

Contributions are welcome. Our release model uses [Git Flow approach](http://danielkummer.github.io/git-flow-cheatsheet/), so you have to base your branches on `develop` and should have name `feature/name-of-feature` or `hotfix/name-of-hotfix`.

Please contribute your translations, which are located in `lib/lang/` directory. Thank you.