<?php
/*
Copyright (c) 2016, Kristián Feldsam, FELDSAM s.r.o. <info@feldsam.cz>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


/******************************************
* Begin Form configuration
******************************************/

$tform_def_file = "form/template.tform.php";

/******************************************
* End Form configuration
******************************************/

require_once '../../lib/config.inc.php';
require_once '../../lib/app.inc.php';

//* Check permissions for module
$app->auth->check_module_permissions('wizard');
$app->auth->check_module_permissions('sites');

// Loading classes
$app->uses('tpl,tform,tform_actions,tools_sites');
$app->load('tform_actions');

class page_action extends tform_actions {

	function onShowEnd()
	{
		global $app, $conf;
		
		$app->uses('getconf');
		
		if($this->dataRecord["web_server_id"])
		{
			$web_server_id = $this->dataRecord["web_server_id"];
		}
		else
		{
			$default_web_server = $app->db->queryOneRecord("SELECT server_id FROM server WHERE web_server = ? ORDER BY server_id LIMIT 0,1", 1);
			$web_server_id = $default_web_server['server_id'];
		}
		
		$web_config = $app->getconf->get_server_config($web_server_id, 'web');
		
		//* Fill the IPv4 select field
		$sql = "SELECT ip_address FROM server_ip WHERE ip_type = 'IPv4' AND server_id = ".$web_server_id;
		$ips = $app->db->queryAllRecords($sql);
		$ip_select = ($web_config['enable_ip_wildcard'] == 'y')?"<option value='*'>*</option>":"";
		//$ip_select = "";
		if(is_array($ips)) {
			foreach( $ips as $ip) {
				$selected = ($ip["ip_address"] == $this->dataRecord["ip_address"])?'SELECTED':'';
				$ip_select .= "<option value='$ip[ip_address]' $selected>$ip[ip_address]</option>\r\n";
			}
		}
		$app->tpl->setVar("ip_address", $ip_select);
		unset($tmp);
		unset($ips);

		//* Fill the IPv6 select field
		$sql = "SELECT ip_address FROM server_ip WHERE ip_type = 'IPv6' AND server_id = ".$web_server_id;
		$ips = $app->db->queryAllRecords($sql);
		$ip_select = "<option value=''></option>";
		//$ip_select = "";
		if(is_array($ips)) {
			foreach( $ips as $ip) {
				$selected = ($ip["ip_address"] == $this->dataRecord["ipv6_address"])?'SELECTED':'';
				$ip_select .= "<option value='$ip[ip_address]' $selected>$ip[ip_address]</option>\r\n";
			}
		}
		$app->tpl->setVar("ipv6_address", $ip_select);
		unset($tmp);
		unset($ips);
		
		parent::onShowEnd();
	}

}

$page = new page_action;
$page->onLoad();
